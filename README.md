[[_TOC_]]

# gitlab-com

Kubernetes Workload configurations for GitLab.com

## Documentation

* [CONTRIBUTING.md](CONTRIBUTING.md)
* [DEPLOYMENT.md](DEPLOYMENT.md)
* [TROUBLESHOOTING.md](TROUBLESHOOTING.md)

:warning: **WARNING** :warning:

The following are _NOT_ allowed this repository:
* Files that contain secrets in plain text

## Services

The following services are managed by this Chart:

| Service | Upgrades |
| --- | --- |
| [API](https://gitlab.com/gitlab-org/gitlab) ([readiness](https://gitlab.com/gitlab-com/gl-infra/readiness/-/tree/master/api-k8s-migration)) | Auto-deploy pipeline created from a pipeline trigger from the deployer pipeline |
| [KAS](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent) ([readiness](https://gitlab.com/gitlab-com/gl-infra/readiness/-/blob/master/kubernetes-agent/index.md)) | Upgrades are performed manually by setting a version in the appropriate [values.yaml](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/027a4ae0eb15c83cae9fcb4eaaad7f833ee76971/releases/gitlab/values/pre.yaml.gotmpl#L203) |
| [Web](https://gitlab.com/gitlab-org/gitlab) ([readiness](https://gitlab.com/gitlab-com/gl-infra/readiness/-/blob/master/web-k8s-migration/index.md)) | Auto-deploy pipeline created from a pipeline trigger from the deployer pipeline |
| [Websockets](https://gitlab.com/gitlab-org/gitlab) ([readiness](https://gitlab.com/gitlab-com/gl-infra/readiness/-/tree/master/websockets)) | Auto-deploy pipeline created from a pipeline trigger from the deployer pipeline |
| [Git](https://gitlab.com/gitlab-org/gitlab) ([readiness](https://gitlab.com/gitlab-com/gl-infra/readiness/-/blob/master/git-https-websockets/index.md)) | Auto-deploy pipeline created from a pipeline trigger from the deployer pipeline |
| [Mailroom](https://gitlab.com/gitlab-org/gitlab-mail_room) ([readiness](https://gitlab.com/gitlab-com/gl-infra/readiness/-/blob/master/mailroom/overview.md)) | Upgrades are performed manually by setting a version in [values.yaml](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/7bd15324144a2c85699bf685fb606b6dd7c92975/releases/gitlab/values/values.yaml.gotmpl#L1076-1080) ([release template](https://gitlab.com/gitlab-org/gitlab-mail_room/-/blob/master/.gitlab/issue_templates/Release.md)). |
| [Container Registry](https://gitlab.com/gitlab-org/container-registry) ([readiness](https://gitlab.com/gitlab-com/gl-infra/readiness/-/blob/master/registry-gke/overview.md)) | Upgrades are performed manually by setting a version in [environments.yaml](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/027a4ae0eb15c83cae9fcb4eaaad7f833ee76971/bases/environments.yaml#L68) ([release template](https://gitlab.com/gitlab-org/container-registry/-/blob/master/.gitlab/issue_templates/Release%20Plan.md)). |
| [Sidekiq](https://gitlab.com/gitlab-org/gitlab) ([readiness](https://gitlab.com/gitlab-com/gl-infra/readiness/-/blob/master/sidekiq/index.md)) | Auto-deploy pipeline created from a pipeline trigger from the deployer pipeline |

## GitLab Environments Configuration

While `gstg` and `gprd` are single environments on their own, we are leveraging
helmfile environments to segregate configuration changes to each cluster that
participates in each environment.  As such `gprd` and `gstg` expand their
environment configurations into 1 per cluster.

On merge, configuration changes will be deployed to the following environments:

| Environment | URL | Cluster |
| ----------- | --- | ------- |
| `pre`       | `https://pre.gitlab.com`     | pre-gitlab-gke |
| `gstg`      | `https://staging.gitlab.com` | gstg-gitlab-gke |
| `gstg`      | `https://staging.gitlab.com` | gstg-us-east1-b |
| `gstg`      | `https://staging.gitlab.com` | gstg-us-east1-c |
| `gstg`      | `https://staging.gitlab.com` | gstg-us-east1-d |
| `gprd`      | `https://gitlab.com`         | gprd-gitlab-gke |
| `gprd`      | `https://gitlab.com`         | gprd-us-east1-b |
| `gprd`      | `https://gitlab.com`         | gprd-us-east1-c |
| `gprd`      | `https://gitlab.com`         | gprd-us-east1-d |

## GitLab CI/CD Configuration

Our GitLab CI/CD configuration for this repository is written in [jsonnet](https://jsonnet.org).
Note that the `.gitlab-ci.yml` for this repository is not to be modified by
hand, it is instead to be modified through the corresponding source file
`.gitlab-ci.jsonnet`, and then rendered out using `make generate-ci-config`. When
doing a merge request to change the CI/CD configuration of this repository, make
sure the MR includes both files.

### GitLab CI/CD Variables Configuration

#### Input Variables

Each of the below variables is applied to the environment defined above

| Variable                         | Description
| --------                         | --------
| `CLUSTER`                        | Name of the GKE cluster, ex: `gstg-gitlab-gke` or `gstg-us-east1-b`
| `REGION `                        | Name of the region or zone of the cluster, ex: `us-east1` or `us-east1-b`
| `PROJECT`                        | Name of the project, ex: `gitlab-staging-1`
| `GOOGLE_APPLICATION_CREDENTIALS` | Service Account key used for CI for read or write operations to the cluster

#### Ops Instance Specific

| Variable | Description |
| -------- | ----------- |
| `OPS_API_TOKEN`       | Token utilized by the ops.gitlab.com instance to make API calls on behalf of the CI jobs. |
| `CLUSTER_SKIP`        | Input the name of a single cluster to be skipped during the next run of a CI JOB.  This is to be used [in cases of emergency or maintenance](./TROUBLESHOOTING.md#skipping-cluster-deployments) |
| `EXPEDITE_DEPLOYMENT` | Skips select processes and CI Jobs to push a configuration change out to production faster than normal. This is to be used [in cases of emergency](./TROUBLESHOOTING.md#in-case-of-emergency) |

### Access to GitLab production website blocked in CI

Please note that the tooling in this repository specifically sometimes blocks access to the following URLs when running CI Jobs, only during the execution of helm/helmfile

* gitlab.com
* registry.gitlab.com
* charts.gitlab.io

The rationale behind this is to avoid a situation where our deployment tooling to deploy GitLab.com on Kubernetes is dependant on GitLab.com being available. During
an outage where we might might need to use this repository to deploy an upgrade/fix to GitLab.com, we don't want this to fail because some part of GitLab.com is unavailable.

The CI job will disable access to these urls if the following conditions are met

* We have the CI environment variable 'GITLAB_ACCESS_DISABLE' set. This is typically set as a Global CI variable in the projects configuration, and allows us to globally enable/disable this functionality at will.

## GitLab Secrets

All Kubernetes secrets are managed by the External Secrets operator and pulled from Hashicorp Vault. See the [`gitlab-external-secrets` release](releases/gitlab-external-secrets/) and the [Vault usage runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/vault/usage.md) to learn how to access Vault and manage secrets.

## Flagger Canary Deployments
On certain environments (currently `preprod`) where `Flagger` setups are available, `Canary` custom resource deployments can be set up for service deployments. These are **not** the same as the canary deployments we currently have in the `cny` stage (vs `main` stage) for environments (`gstg` and `gprd`).

[`Flagger`](https://github.com/fluxcd/flagger) is a progressive delivery tool that automates the release process for applications running on Kubernetes. It is set up to work along `Istio` as part of [the effort to develop ability to dynamically route traffic within different cluster deployments](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/962). The `flagger-canary` custom resource can be set up for `gitlab` services in order to introduce more flexibility with traffic routing and deployment strategies during the upgrade processes. Refer to the [Canary resource setup under `gitlab-extras`](releases/gitlab-extras/values.yaml.gotmpl) and the [`README` for more details](releases/gitlab-extras/FLAGGER-CANARY.md).

## Decisions

Read about how we've come to decide how this repository is setup by viewing our [design document](https://about.gitlab.com/handbook/engineering/infrastructure/library/kubernetes/configuration/).

## Working locally

Local development is not something that currently works very well.  An issue
exists to address this:
https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/381

You are able to get an overview of the configuration manifests for an environment using `./bin/k-ctl template`. However, in order for this to work, you either need to have access to the running Kubernetes
cluster you wish to template manifests for, or manually pass in some environment variables to the template command (which are fake and just used to insert something into the needed data). All manifests are output
into the `manifests` folder of the repository.

Some examples to generate manifests are

```shell
# Generate manifests for staging regional cluster, using actual container versions currently running in staging (requires access to Kubernetes cluster)
./bin/k-ctl -e gstg template

# Generate manifests for staging regional cluster, with fake version data (does not require access to Kubernetes cluster). Note this will force the registry migration job to not be run.
GITLAB_IMAGE_TAG=15.test SKIP_REGISTRY_MIGRATION=true ./bin/k-ctl -e gstg template

# Generate manifests for staging regional cluster, with fake version data (does not require access to Kubernetes cluster). Make sure registry migration job is run
# First change bases/environments.yaml to upgrade registry version
GITLAB_IMAGE_TAG=15.test ./bin/k-ctl -e gstg template
```

### Prerequisites

Complete the [Workstation setup](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/kube/k8s-oncall-setup.md#workstation-setup-for-k-ctl) steps described in the [k8s-operations runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/kube/k8s-operations.md).

## Bootstrapping new clusters

### Creating a new environment

Every cluster must have a unique environment for Helm, there should be a new environment defined in https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/master/bases/environments.yaml that inherits the right values depending on whether it is staging or production.

After the environment is defined, CI jobs will need to be created in the [gitlab-ci.yml](https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/master/.gitlab-ci.yml) for gitlab-helmfiles.

See [Example MR for the production zonal clusters](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/448)

### Apply configuration locally to the cluster

It's useful to apply configuration locally using `k-ctl` for the first time, to work out any issues that may arise.
Before applying you will need to set the following environment variables:

```shell
CLUSTER=<cluster name>
REGION=<region or zone name>
```

Then apply using `k-ctl`

```shell
./bin/k-ctl -e <env name> apply
```

## Setting Chart Version

We vendor the `gitlab` chart into this repo, located in `vendor/charts`. This allows
us to minimise the amount of external dependencies invoked at runtime, and allow
easier understanding of this repo by including all "code" needed inside it.

We use a tool called [vendir](https://carvel.dev/vendir) to handle the process
of "vendoring" the charts into this repo (and checking they are the same as
their upstream versions during merge requests).

There are two ways for which one can accomplish this task.  Semi-automated and
manual.

### Semi-Automated

The Semi-Automated method automatically opens MR's using the latest ref
available from our helm charts.  This can be kickstarted out of band by running
the scheduled pipeline:
[`auto-chart-bump-weekly`](https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/pipeline_schedules)

### Manual

This is a great option if we don't want the latest or are targeting a specific
environment.

Edit `vendir.yml` file, and look for the `ref` field under the chart directory
you are interested (these are in the format `gitlab/${environment name}`. Change
the ref to the new git ref (from the charts repository) you wish to use, then
run the command `vendir sync` which will vendor the new copy locally. Then feel
free to do an MR with these changes to actually get the change applied.

An example to commit the new chart for in `gstg` for the `gitlab` chart

```shell
# First login to the `ops` OCI registry if you have not done so already
docker login registry.ops.gitlab.net
# Enter your ops.gitlab.net username as the username, and the password
# will be an Personal Access Token you have generated on ops.gitlab.net
# with the `read_registry` permissions

# Now lets checkout a branch for the MR with the chart bump
git checkout -b username/bump-chart-gitlab-in-gstg
# edit vendir.yml changing `ref` for the `gitlab/gstg` path to the new SHA
vendir sync
# manually sync helm deps
cd vendor/charts/gitlab/gstg
helm dep update
cd ../../../..
git add vendor/charts/gitlab/gstg vendir.yml vendir.lock.yml
git commit -m "Bump chart in gstg"
# You are now ready to push and open an MR
```

It is expected that if you are bumping the version in one environment you will
take responsibility for bumping all other environments in due course.  If your
focus/requirement is only `gstg` and `gprd`, you should probably do `pre` when
you do `gstg` (same MR) unless there are extenuating circumstances (e.g.
other work ongoing on pre that shouldn't be interrupted, etc.)
